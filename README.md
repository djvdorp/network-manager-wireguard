# [NetworkManager VPN Plugin: Wireguard](https://github.com/max-moser/network-manager-wireguard) Packages

### Add repo signing key to apt

```bash
wget -q -O - https://packaging.gitlab.io/network-manager-wireguard/gpg.key | sudo apt-key add -
```

or

```bash
curl -sL https://packaging.gitlab.io/network-manager-wireguard/gpg.key | sudo apt-key add - 
```

### Add repo to apt

```bash
echo "deb [arch=amd64] https://packaging.gitlab.io/network-manager-wireguard bionic main" | sudo tee /etc/apt/sources.list.d/morph027-network-manager-wireguard-bionic.list
```
